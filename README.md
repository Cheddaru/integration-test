# Integration test

This node project is an exercise on integration testing

## Endpoints (and parameters)

* */* - Welcome message
* */rgb-to-hex* - converts rgb values to hex string
  * *r* - 0-255
  * *g* - 0-255
  * *b* - 0-255
* */hex-to-rgb* - converts hex values to rgb list
  * *hex* - colorstring (with or without # (must be encoded))