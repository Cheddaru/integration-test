/**
 * Padding output to match 2 characters always.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) =>{
    return (hex.length === 1 ? "0" + hex : hex)
}

module.exports = {
    /**
     * Converts the RGB values to a Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex string
     */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16)
        const greenHex = green.toString(16)
        const blueHex = blue.toString(16)
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex)
        return hex
    },
    /**
     * Converts hex string to RGB values
     * @param {string} hex
     * @returns {list} RGB list
     */
    hexToRgb: (hex) => {
        // Remove the leading # if it exists
        hex = hex.replace("#", "")
        // Do the actual conversion
        const red = parseInt(hex.substring(0,2), 16)
        const green = parseInt(hex.substring(2,4), 16)
        const blue = parseInt(hex.substring(4,6), 16)
        return [red,green,blue]
    }
}