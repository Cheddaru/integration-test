

const converter = require("./converter")
const express = require('express')
const app = express()
const PORT = 3000

app.get("/", (req, res) => {
    res.send("Welcome!")
})
app.get("/rgb-to-hex", (req, res) => {
    const red = req.query.r
    const green = req.query.g
    const blue = req.query.b
    const hex = converter.rgbToHex(parseInt(red, 10), parseInt(green, 10), parseInt(blue, 10));
    res.send(hex)
})
app.get("/hex-to-rgb", (req, res) => {
    //if request has "#" in it, it must be encoded properly!
    const hex = req.query.hex
    const rgb = converter.hexToRgb(hex)
    res.send(rgb)
})

if(process.env.NODE_ENV === 'test') {
    module.exports = app
} else {
    app.listen(PORT, () => console.log("Server listening..."))
}

console.log("NODE_ENV: " + process.env.NODE_ENV)