const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3000;

// Start the server, test conversion endpoints and stop the server

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to Hex conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("Returns a correct HEX string", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(response.body).to.equal("#ff0000")
                done()
            })
        })
    });
    describe("Hex to RGB conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/hex-to-rgb?hex=%23ff0000";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("Returns a correct RGB list", (done) => {
            const url = baseurl + "/hex-to-rgb?hex=%23ff0000";
            request(url, (error, response, body) => {
                expect(response.body).to.equal("[255,0,0]")
                done()
            })
        })
    });
    after("Stop server", (done) => {
        server.close();
        done();
    });
});